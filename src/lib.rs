use std::cell::UnsafeCell;
use std::future::{poll_fn, Future};
use std::mem::MaybeUninit;
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering};
use std::sync::Arc;
use std::task::Poll;

use futures::task::AtomicWaker;
use tokio::io::{AsyncRead, AsyncWrite};

pub struct BufReader {
    inner: Arc<Inner>,
}

struct Iter<I: Iterator<Item = u8>> {
    iter: I,
}

impl<I: Iterator<Item = u8>> Iterator for Iter<I> {
    type Item = u8;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }
}

impl<I: Iterator<Item = u8>> Iter<I> {
    unsafe fn new(iter: I) -> Self {
        Self { iter }
    }
}

impl<I: Iterator<Item = u8>> ExactSizeIterator for Iter<I> {}

impl BufReader {
    fn try_read(&mut self, buf: &mut tokio::io::ReadBuf<'_>) -> bool {
        match self.inner.get_readable() {
            None => true,
            Some((first, second)) => unsafe {
                let first = &*first;
                let second = &*second;
                if first.len() + second.len() == 0 {
                    return false;
                }
                let to_cpy_first = first.len().min(buf.remaining());
                let mut consumed = to_cpy_first;
                buf.put_slice(&first[..to_cpy_first]);
                if buf.remaining() != 0 {
                    let to_cpy_second = second.len().min(buf.remaining());
                    buf.put_slice(&second[..to_cpy_second]);
                    consumed += to_cpy_second;
                }
                self.inner.consume(consumed);
                true
            },
        }
    }

    // get_slices returns the slices that make up the
    pub fn get_slices(&self) -> Option<(&[u8], &[u8])> {
        self.inner
            .get_readable()
            .map(|(r0, r1)| unsafe { (&*r0, &*r1) })
    }
    pub fn iter(&self) -> impl Iterator<Item = u8> + ExactSizeIterator + '_ {
        let (r0, r1) = self.get_slices().unwrap_or((&[], &[]));
        let iter = r0.iter().chain(r1).copied();
        unsafe { Iter::new(iter) }
    }

    pub fn consume(&mut self, amt: usize) -> Result<usize, usize> {
        let available = self.inner.available();
        if self.inner.available() < amt {
            Err(available)
        } else {
            unsafe {
                self.inner.consume(amt);
            }
            Ok(available - amt)
        }
    }

    pub fn poll_readable(&mut self, cx: &mut std::task::Context<'_>) -> Poll<Option<usize>> {
        self.inner.poll_readable(cx)
    }

    pub fn readable(&mut self) -> impl Future<Output = Option<usize>> + Unpin + '_ {
        poll_fn(|cx| self.inner.poll_readable(cx))
    }
}

impl Drop for BufReader {
    fn drop(&mut self) {
        self.inner.finish_reads();
    }
}
impl AsyncRead for BufReader {
    fn poll_read(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> std::task::Poll<std::io::Result<()>> {
        if buf.remaining() == 0 {
            return Poll::Ready(Ok(())); // TODO: is this valid
        }

        if self.try_read(buf) {
            return Poll::Ready(Ok(()));
        }

        self.inner.read_cond_waker.register(cx.waker());
        if self.try_read(buf) {
            self.inner.read_cond_waker.take(); // TODO: is this needed
            Poll::Ready(Ok(()))
        } else {
            Poll::Pending
        }
    }
}

pub struct BufWriter {
    inner: Arc<Inner>,
}

impl BufWriter {
    fn try_write(&self, buf: &[u8]) -> Option<usize> {
        self.inner.get_writeable().map(|(first, second)| unsafe {
            let first = &mut *first;
            let second = &mut *second;
            let ds_iter = first.iter_mut().chain(second);
            let ds_iter = ds_iter.zip(buf);
            let (written, upper) = ds_iter.size_hint();
            assert_eq!(Some(written), upper);
            if written == 0 {
                return 0;
            }
            ds_iter.for_each(|(d, s)| {
                d.write(*s);
            });
            self.inner.write(written);
            written
        })
    }
}

impl Drop for BufWriter {
    fn drop(&mut self) {
        self.inner.finish_writes();
    }
}

impl AsyncWrite for BufWriter {
    fn poll_write(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, std::io::Error>> {
        if buf.is_empty() {
            return Poll::Ready(Ok(0));
        }
        match self.try_write(buf) {
            None => return Poll::Ready(Err(std::io::ErrorKind::BrokenPipe.into())),
            Some(0) => {}
            Some(u) => return Poll::Ready(Ok(u)),
        }

        self.inner.write_cond_waker.register(cx.waker());
        match self.try_write(buf) {
            None => Poll::Ready(Err(std::io::ErrorKind::BrokenPipe.into())),
            Some(0) => Poll::Pending,
            Some(u) => Poll::Ready(Ok(u)),
        }
    }
    fn poll_flush(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        if self.inner.available() == 0 {
            return Poll::Ready(Ok(()));
        }

        self.inner.write_cond_waker.register(cx.waker());
        if self.inner.available() == 0 {
            Poll::Ready(Ok(()))
        } else {
            Poll::Pending
        }
    }

    fn poll_shutdown(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        self.inner.finish_writes();
        self.poll_flush(cx)
    }
}

struct Inner {
    read_cond_waker: AtomicWaker,
    write_cond_waker: AtomicWaker,
    written: AtomicUsize,
    consumed: AtomicUsize,
    buf: *const UnsafeCell<[MaybeUninit<u8>]>,
    capacity: usize,
    write_done: AtomicBool,
    read_done: AtomicBool,
}

unsafe impl Sync for Inner {}
unsafe impl Send for Inner {}

impl Inner {
    fn consumed(&self) -> usize {
        self.consumed.load(Ordering::SeqCst)
    }
    unsafe fn consume(&self, consumed: usize) {
        self.consumed.fetch_add(consumed, Ordering::SeqCst);
        self.write_cond_waker.wake();
    }

    fn written(&self) -> usize {
        self.written.load(Ordering::SeqCst)
    }
    unsafe fn write(&self, writen: usize) {
        self.written.fetch_add(writen, Ordering::SeqCst);
        self.read_cond_waker.wake();
    }

    fn write_done(&self) -> bool {
        self.write_done.load(Ordering::SeqCst)
    }
    fn finish_writes(&self) {
        self.write_done.store(true, Ordering::SeqCst);
        self.read_cond_waker.wake();
    }
    fn read_done(&self) -> bool {
        self.read_done.load(Ordering::SeqCst)
    }
    fn finish_reads(&self) {
        self.read_done.store(true, Ordering::SeqCst);
        self.write_cond_waker.wake();
    }
    fn wrap_index(&self, index: usize) -> usize {
        debug_assert!(
            self.capacity.is_power_of_two(),
            "wrap_index is only valid for powers of two"
        );
        index & (self.capacity - 1)
    }

    fn slice_ptr(&self) -> *mut [MaybeUninit<u8>] {
        UnsafeCell::raw_get(self.buf)
    }
    fn ptr(&self) -> *mut MaybeUninit<u8> {
        self.slice_ptr() as *mut MaybeUninit<u8>
    }
    /*fn buffer_as_slice(&self) -> &[MaybeUninit<u8>] {
        unsafe {
            std::slice::from_raw_parts(self.ptr() as *mut MaybeUninit<u8>, self.capacity)
        }
    }*/

    /// Returns the number of bytes available to read.
    fn available(&self) -> usize {
        let consumed = self.consumed();
        let written = self.written();
        written.wrapping_sub(consumed)
    }

    #[allow(clippy::type_complexity)]
    fn get_writeable(&self) -> Option<(*mut [MaybeUninit<u8>], *mut [MaybeUninit<u8>])> {
        let mut consumed = self.consumed();
        let written = self.written();
        let mut available = written.wrapping_sub(consumed);
        let ptr = self.slice_ptr();
        let empty = std::ptr::slice_from_raw_parts_mut(ptr as *mut MaybeUninit<u8>, 0);
        if available == self.capacity {
            if self.read_done() {
                // need to make sure that consumed and read_done
                // were not updated since first read from consumed
                consumed = self.written();
                available = written.wrapping_sub(consumed);
                if available == 0 {
                    return None;
                }
            } else {
                return Some((empty, empty));
            }
        }

        let ptr = self.ptr();
        let tail = self.wrap_index(consumed);
        let head = self.wrap_index(written);
        unsafe {
            let first_start = ptr.add(head);
            let (first_len, second) = if tail <= head {
                let second = std::ptr::slice_from_raw_parts_mut(ptr, tail);
                (self.capacity - head, second)
            } else {
                // we know the first is continious
                (tail - head, empty)
            };
            let first = std::ptr::slice_from_raw_parts_mut(first_start, first_len);
            Some((first, second))
        }
    }

    /// If `None` is returned then there are not readable bytes and the
    /// `BufWriter` has been shutdown/dropped.
    /// The `Some` the two slice represent the two slice in the circular buffer.
    /// # Safety
    /// The *const [u8]s returned are valid until `self.consume` is called.
    fn get_readable(&self) -> Option<(*const [u8], *const [u8])> {
        let (written, consumed) = self.get_read_positions()?;
        let available = written.wrapping_sub(consumed);
        let empty = &[] as *const [u8];
        if available == 0 {
            return Some((empty, empty));
        }

        let ptr = self.ptr();
        let tail = self.wrap_index(consumed);
        let head = self.wrap_index(written);
        unsafe {
            // SAFETY: These slices have already been written to
            let first_start = ptr.add(tail);
            let (first_len, second) = if head <= tail {
                let second = std::ptr::slice_from_raw_parts(ptr, head);
                (self.capacity - tail, second)
            } else {
                // we know the first is continious
                (head - tail, &[][..] as *const _)
            };
            let first = std::ptr::slice_from_raw_parts_mut(first_start, first_len);
            let first = &*(first as *const [u8]);
            let second = &*(second as *const [u8]);
            Some((first, second))
        }
    }

    fn get_read_positions(&self) -> Option<(usize, usize)> {
        let consumed = self.consumed();
        let mut written = self.written();
        let mut available = written.wrapping_sub(consumed);
        if available == 0 && self.write_done() {
            // need to make sure that written and write_done
            // were not updated since first read from writen
            written = self.written();
            available = written.wrapping_sub(consumed);
            if available == 0 {
                return None;
            }
        }
        Some((written, consumed))
    }

    fn poll_readable(&self, cx: &mut std::task::Context<'_>) -> Poll<Option<usize>> {
        let diff_fn = |(written, consumed): (usize, _)| written.wrapping_sub(consumed);
        match self.get_read_positions().map(diff_fn) {
            None => return Poll::Ready(None),
            Some(a) if a != 0 => return Poll::Ready(Some(a)),
            Some(_) => {}
        }

        self.read_cond_waker.register(cx.waker());
        match self.get_read_positions().map(diff_fn) {
            None => Poll::Ready(None),
            Some(a) if a != 0 => Poll::Ready(Some(a)),
            Some(_) => Poll::Pending,
        }
    }
}
impl Drop for Inner {
    fn drop(&mut self) {
        unsafe {
            let layout = std::alloc::Layout::array::<u8>(self.capacity).unwrap();
            let ptr: *mut [MaybeUninit<u8>] = UnsafeCell::raw_get(self.buf);
            std::alloc::dealloc(ptr as *mut u8, layout);
        }
    }
}

pub fn make_buffer(capacity: usize) -> (BufWriter, BufReader) {
    assert_ne!(capacity, 0);
    assert!(capacity < isize::MAX as usize);
    let capacity = capacity.next_power_of_two();
    let buf = unsafe {
        let layout = std::alloc::Layout::array::<u8>(capacity).unwrap();

        // SAFETY: This pointer is stored in
        let ptr = std::alloc::alloc(layout);
        assert!(!ptr.is_null());

        let ptr = std::ptr::slice_from_raw_parts_mut(ptr, capacity);

        // SAFETY: This is valid because UnsafeCell/MaybeUninit uses a repr(transparent).
        let buf: *mut UnsafeCell<[MaybeUninit<u8>]> = std::mem::transmute(ptr);
        let buf_ref = &*UnsafeCell::raw_get(buf);
        debug_assert_eq!(buf_ref.len(), capacity);
        buf
    };
    let inner = Inner {
        read_cond_waker: AtomicWaker::new(),
        write_cond_waker: AtomicWaker::new(),
        buf,
        capacity,
        written: AtomicUsize::new(0),
        consumed: AtomicUsize::new(0),
        read_done: AtomicBool::new(false),
        write_done: AtomicBool::new(false),
    };
    let inner = Arc::new(inner);
    let writer = BufWriter {
        inner: inner.clone(),
    };
    let reader = BufReader { inner };
    (writer, reader)
}

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;
    use tokio::io::{AsyncReadExt, AsyncWriteExt};

    // This asserts that the buffers returned by
    // `Inner.get_readable` and `Inner.get_writeable` are complements
    // to each other.
    fn assert_buffer_validity(reader: &mut BufReader) {
        let available = reader.inner.available();
        let (read_0, read_1) = reader.inner.get_readable().unwrap();
        let (r0_range, r1_range) = unsafe {
            // It is safe to downgrade to MaybeUninit
            let first = &*(read_0 as *const [MaybeUninit<u8>]);
            let second = &*(read_1 as *const [MaybeUninit<u8>]);
            assert_eq!(first.len() + second.len(), available);
            (first.as_ptr_range(), second.as_ptr_range())
        };

        let (write_0, write_1) = reader.inner.get_writeable().unwrap();
        let (w0_range, w1_range) = unsafe {
            let first = &*write_0;
            let second = &*write_1;
            assert_eq!(
                first.len() + second.len(),
                reader.inner.capacity - available
            );
            (first.as_ptr_range(), second.as_ptr_range())
        };
        let whole_range = unsafe {
            let buf = &*reader.inner.slice_ptr();
            buf.as_ptr_range()
        };

        assert!(
            r1_range.is_empty() || w1_range.is_empty(),
            "Only either a read or write slice can be discontinous"
        );

        if !r0_range.is_empty() && !r1_range.is_empty() {
            // readable range is discontinous
            assert_eq!(r0_range.end, whole_range.end);
            assert_eq!(r1_range.start, whole_range.start);
            if !w0_range.is_empty() {
                assert_eq!(w0_range.end, r0_range.start);
                assert_eq!(r1_range.end, w0_range.start);
            }
        } else if !w0_range.is_empty() && !w1_range.is_empty() {
            // writable range is discontinous
            assert_eq!(w0_range.end, whole_range.end);
            assert_eq!(w1_range.start, whole_range.start);
            if !r0_range.is_empty() {
                assert_eq!(r0_range.end, w0_range.start);
                assert_eq!(w1_range.end, r0_range.start);
            }
        } else {
            if r0_range.is_empty() {
                assert_eq!(w0_range, whole_range);
            } else {
                assert!(r0_range.start == whole_range.start || r0_range.end == whole_range.end);
            }
            if w0_range.is_empty() {
                assert_eq!(r0_range, whole_range);
            } else {
                assert!(w0_range.start == whole_range.start || w0_range.end == whole_range.end);
            }
        }
    }

    #[tokio::test]
    async fn simple_send_recv() {
        let (mut writer, mut reader) = make_buffer(32);
        let buf: Vec<u8> = (0..32).map(usize::to_be_bytes).flatten().collect();
        let len = buf.len();
        let read_handle = tokio::task::spawn(async move {
            let mut recv_buf = vec![0; len];
            let len = reader.read_exact(&mut recv_buf).await.unwrap();
            recv_buf.truncate(len);
            recv_buf
        });
        writer.write_all(&buf).await.unwrap();
        let recv_buf = read_handle.await.unwrap();
        assert_eq!(buf, recv_buf);
    }

    #[tokio::test]
    async fn jagged_send_recv() {
        const BUF_SIZE: usize = 32;
        const HALF_SIZE: usize = BUF_SIZE / 2;
        let (mut writer, mut reader) = make_buffer(BUF_SIZE);
        assert_buffer_validity(&mut reader);
        let buf: Vec<u8> = (0..32).map(usize::to_be_bytes).flatten().collect();
        assert!(matches!(writer.write(&buf[..BUF_SIZE]).await, Ok(BUF_SIZE)));
        assert_buffer_validity(&mut reader);

        let mut read_buf = [0; BUF_SIZE + HALF_SIZE];
        assert_eq!(reader.readable().await, Some(BUF_SIZE));
        assert!(matches!(
            reader.read(&mut read_buf[..HALF_SIZE]).await,
            Ok(HALF_SIZE)
        ));
        assert_buffer_validity(&mut reader);

        assert!(matches!(reader.readable().await, Some(HALF_SIZE),));
        assert!(matches!(
            writer.write(&buf[BUF_SIZE..BUF_SIZE * 2]).await,
            Ok(HALF_SIZE)
        ));
        assert_buffer_validity(&mut reader);

        assert!(matches!(reader.readable().await, Some(BUF_SIZE)));
        assert!(matches!(
            reader.read(&mut read_buf[HALF_SIZE..]).await,
            Ok(BUF_SIZE)
        ));
        assert_buffer_validity(&mut reader);

        drop(writer);
        assert!(matches!(reader.readable().await, None));
    }

    fn gen(idx: usize) -> impl Iterator<Item = u8> {
        ((idx as u32 / 4)..)
            .map(u32::to_be_bytes)
            .flatten()
            .skip(idx % 4)
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn consumed() {
        const TO_SEND: usize = 1024;
        const BUF_SIZE: usize = 32;
        let (mut writer, mut reader) = make_buffer(BUF_SIZE);
        let mut buf = [0; TO_SEND];
        gen(0).zip(&mut buf).for_each(|(s, d)| *d = s);

        let write_handler = tokio::task::spawn(async move {
            writer.write_all(&buf).await.unwrap();
        });

        let mut recv_buf = [0; TO_SEND];
        let mut slice = &mut recv_buf[..];
        while !slice.is_empty() {
            let readable = reader.readable().await.unwrap();
            let iter = reader.iter();
            let to_consume = iter.len();
            assert!(to_consume >= readable);
            iter.zip(slice.iter_mut()).for_each(|(s, d)| *d = s);
            reader.consume(to_consume).unwrap();
            slice = &mut slice[to_consume..];
        }

        assert_eq!(buf, recv_buf);
        assert!(reader.readable().await.is_none());
        write_handler.await.unwrap();
    }

    use rand::distributions::Uniform;
    use rand::prelude::*;
    use rand::rngs::SmallRng;
    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn multi_thread_send_recv() {
        const TO_SEND: usize = 1024 * 1024 * 4;
        const BUF_SIZE: usize = 1024;

        let (mut writer, mut reader) = make_buffer(BUF_SIZE);

        let sampler = Uniform::from(0..=1024);
        let write_handler = tokio::task::spawn(async move {
            let mut w_iterations = 0;
            let mut rng = SmallRng::from_entropy();
            let mut buf = [0; BUF_SIZE];

            let mut written = 0;
            while written < TO_SEND {
                let to_write = sampler.sample(&mut rng).min(TO_SEND - written);
                gen(written)
                    .take(to_write)
                    .zip(&mut buf)
                    .for_each(|(s, d)| *d = s);
                written += writer.write(&buf[..to_write]).await.unwrap();
                w_iterations += 1;
            }
            w_iterations
        });

        let mut rng = SmallRng::from_entropy();
        let mut buf = [0; BUF_SIZE];

        let mut read = 0;
        let mut r_iterations = 0;
        while read < TO_SEND {
            let to_read = sampler.sample(&mut rng);
            let just_read = reader.read(&mut buf[..to_read]).await.unwrap();
            for (i, (e, g)) in gen(read).take(just_read).zip(&buf[..just_read]).enumerate() {
                assert_eq!(e, *g, "Byte {} mismatched", i + read);
            }
            read += just_read;
            r_iterations += 1;
        }
        eprintln!(
            "Read iterations {}, average: {}",
            r_iterations,
            TO_SEND as f64 / r_iterations as f64
        );
        let w_iterations = write_handler.await.unwrap();
        eprintln!(
            "Write iterations {}, average: {}",
            w_iterations,
            TO_SEND as f64 / w_iterations as f64
        );
    }
}
